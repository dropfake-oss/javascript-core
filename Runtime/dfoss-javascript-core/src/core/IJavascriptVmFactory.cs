using System;

namespace Javascript.Core
{
	public interface IJavascriptVmFactory : IDisposable
	{
		IJavascriptVirtualMachine CreateJavascriptVirtualMachine(string id);
		bool RemoveJavascriptVirtualMachine(string id);
	}
}
