namespace Javascript.Core
{
	public interface IJavascriptRunner : IJavascriptVmManager
	{
		void Update(int elapsedMs);
	}
}
