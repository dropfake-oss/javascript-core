using System;

namespace Javascript.Core
{
	public interface IJavascriptConsole
	{
		void log(params object[] args);
		void warn(params object[] args);
		void error(params object[] args);
	}

	public class DefaultJavascriptConsole : IJavascriptConsole
	{
		private string GetLogText(params object[] args)
		{
			// TPC: this is probably not optimal for memory/performance
			System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
			foreach (var obj in args)
			{
				stringBuilder.Append((obj != null) ? obj.ToString() : "null");
				stringBuilder.Append(" "); // by default javascript comma delimited strings auto-add a space
			}
			return stringBuilder.ToString();
		}

		public void log(params object[] args)
		{
			var logText = GetLogText(args);
			Console.WriteLine("L: " + logText);
		}

		public void warn(params object[] args)
		{
			var originalForeColor = Console.ForegroundColor;
			try
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				var logText = GetLogText(args);
				Console.WriteLine("W: " + logText);
			}
			finally
			{
				Console.ForegroundColor = originalForeColor;
			}
		}

		public void error(params object[] args)
		{
			var originalForeColor = Console.ForegroundColor;
			try
			{
				Console.ForegroundColor = ConsoleColor.Red;
				var logText = GetLogText(args);
				Console.WriteLine("E: " + logText);
			}
			finally
			{
				Console.ForegroundColor = originalForeColor;
			}
		}
	}
}

