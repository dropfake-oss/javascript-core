using System;
using System.Collections.Generic;

namespace Javascript.Core
{
	public interface IFileSystem
	{
		bool Exists(string path);
		byte[] ReadAllBytes(string path);
		void WriteFile(string path, string contents);
		void WriteFile(string path, byte[] bytes);
	}

	public class DefaultFileSystem : IFileSystem
	{
		public bool Exists(string path)
		{
			return System.IO.File.Exists(path);
		}

		public byte[] ReadAllBytes(string path)
		{
			try
			{
				return System.IO.File.ReadAllBytes(path);
			}
			catch
			{
				//return null;
				// TPC: not sure what benefit there could be in returning null for a file that isn't found
				throw;
			}
		}

		public void WriteFile(string path, string contents)
		{
			System.IO.File.WriteAllText(path, contents);
		}

		public void WriteFile(string path, byte[] bytes)
		{
			System.IO.File.WriteAllBytes(path, bytes);
		}
	}

	public class InMemoryFileSystem : IFileSystem
	{
		private Dictionary<string, string> _fileSystem = new Dictionary<string, string>();

		public bool Exists(string path)
		{
			return _fileSystem.ContainsKey(path);
		}

		public byte[] ReadAllBytes(string path)
		{
			string? fileContents = null;
			if (_fileSystem.TryGetValue(path, out fileContents))
			{
				byte[] bytes = System.Text.Encoding.ASCII.GetBytes(fileContents);
				return bytes;
			}

			throw new System.IO.FileNotFoundException("InMemoryFileSystem: file not found: " + path);
		}

		public void WriteFile(string path, string contents)
		{
			_fileSystem[path] = contents;
		}

		public void WriteFile(string path, byte[] bytes)
		{
			var fileContents = System.Text.Encoding.Default.GetString(bytes);
			_fileSystem[path] = fileContents;
		}
	}
}
