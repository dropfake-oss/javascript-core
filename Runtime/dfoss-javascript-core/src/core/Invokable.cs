using System;

namespace Javascript.Core
{
	public interface Invokable : IDisposable
	{
		void Invoke();
	}

	public class InvokableAction : Invokable
	{
		private Action? _fn;

		public InvokableAction(Action fn)
		{
			_fn = fn;
		}

		public void Invoke()
		{
			try
			{
				if (_fn == null)
				{
					throw new System.NullReferenceException("InvokableAction: Invoke: function has been disposed!  Specifically - InvokeableAction's Dispose() has been called on this instance!");
				}
				_fn();
			}
			catch
			{
				throw;
			}
		}

		public void Dispose()
		{
			_fn = null;
		}
	}
}
