namespace Javascript.Core
{
	public interface IJavascriptVmManager : IJavascriptVmFactory
	{
		IJavascriptVirtualMachine? GetJavascriptVirtualMachine(string id);
		IJavascriptVirtualMachine GetOrCreateJavascriptVirtualMachine(string id);
	}
}
