using System.Collections.Generic;

namespace Javascript.Core
{
	public static class SourceUtils
	{
		public static string LinesToSourceCode(IEnumerable<string> lines)
		{
			var source = "\t" + string.Join("\n\t", lines);
			return source;
		}
	}
}
