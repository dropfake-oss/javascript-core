using System;
using System.Collections.Generic;
using Javascript.Core;

namespace Javascript.Core
{
	public class AssemblyNamespaces
	{
		public System.Reflection.Assembly Assembly;
		public string[] Namespaces;

		public AssemblyNamespaces(System.Reflection.Assembly assembly, string[] namespaces)
		{
			Assembly = assembly;
			Namespaces = namespaces;
		}
	}

	public interface IJavascriptRunnerFactory
	{
		IReadOnlyList<AssemblyNamespaces> AssemblyNamespaces { get; }
		IReadOnlyList<Type> ExtensionMethodsTypes { get; }

		IJavascriptRunner CreateRunner(IReadOnlyList<AssemblyNamespaces> assemblyNamespacesList, IReadOnlyList<Type> extensionMethodsTypes);
		IJavascriptRunner CreateRunner();
	}
}
