using System;

namespace Javascript.Core
{
	public interface IJavascriptVirtualMachine : IDisposable
	{
		#region Timer/Scheduler related functionality
		uint SetTimeout(Action fn, int ms);

		uint SetInterval(Action fn, int ms);

		bool ClearTimer(uint id);

		void UpdateScheduler(int ms);
		#endregion

		event Javascript.Core.UnhandledExceptionEventHandler UnhandledException;

		void SetValue(string name, Type type);
		void SetValue(string name, Object obj);

		object Eval(string moduleName); // returned object may be a javascript engine specific object
		void EvalInMem(string moduleName, string code);

		string ScriptFromFunction(string functionName, string sourceCode, string parameterSignature = "");
		bool RegisterFunction(string functionName, string sourceCode, string parameterSignature = "");
		object InvokeFunction(string functionName, params object[] parameters);
		object RegisterAndInvokeFunction(string functionName, string sourceCode, string parameterSignature = "", params object[] parameters);

		bool IsBool(object obj);
		bool AsBool(object obj);
		bool TryAsBool(object obj, out bool value);

		bool IsNumber(object obj);
		double AsNumber(object obj);
		bool TryAsNumber(object obj, out double value);

		bool IsStruct<T>(object obj) where T : struct;
		T AsStruct<T>(object obj) where T : struct;
		bool TryAsStruct<T>(object obj, out T value) where T : struct;

		bool IsClass<T>(object obj) where T : class;
		T AsClass<T>(object obj) where T : class;
		bool TryAsClass<T>(object obj, out T? value) where T : class;

		void Update(int elapsedMs);
	}
}
