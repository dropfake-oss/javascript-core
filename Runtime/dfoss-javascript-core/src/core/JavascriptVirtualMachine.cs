using System;
using System.Collections.Generic;

namespace Javascript.Core
{
	public abstract class JavascriptVirtualMachine : IJavascriptVirtualMachine
	{
		#region Timer/Scheduler related functionality

		private uint _idgen = default;
		private Dictionary<uint, ulong> _timers = new Dictionary<uint, ulong>();
		protected Javascript.Core.Scheduler _scheduler = new Javascript.Core.Scheduler();

		private uint CreateTimer(Action fn, int ms, bool once)
		{
			var id = ++_idgen;
			var timer = _scheduler.Add(ms, once, new Javascript.Core.InvokableAction(fn));
			_timers.Add(id, timer);
			return id;
		}

		public uint SetTimeout(Action fn, int ms)
		{
			return CreateTimer(fn, ms, true);
		}

		public uint SetInterval(Action fn, int ms)
		{
			return CreateTimer(fn, ms, false);
		}

		public bool ClearTimer(uint id)
		{
			ulong timer;
			if (_timers.TryGetValue(id, out timer))
			{
				_timers.Remove(id);
				_scheduler.Remove(timer);
				return true;
			}
			return false;
		}

		public void UpdateScheduler(int ms)
		{
			_scheduler.Update(ms);
		}

		#endregion

		public abstract void Dispose();

		public abstract event Javascript.Core.UnhandledExceptionEventHandler UnhandledException;

		public abstract void SetValue(string name, Type type);
		public abstract void SetValue(string name, Object obj);

		public abstract object Eval(string moduleName); // returned object may be a javascript engine specific object
		public abstract void EvalInMem(string moduleName, string code);

		public abstract string ScriptFromFunction(string functionName, string sourceCode, string parameterSignature = "");
		public abstract bool RegisterFunction(string functionName, string sourceCode, string parameterSignature = "");
		public abstract object InvokeFunction(string functionName, params object?[] parameters);
		public abstract object RegisterAndInvokeFunction(string functionName, string sourceCode, string parameterSignature = "", params object?[] parameters);

		public abstract bool IsBool(object obj);
		public abstract bool AsBool(object obj);
		public abstract bool TryAsBool(object obj, out bool value);

		public abstract bool IsNumber(object obj);
		public abstract double AsNumber(object obj);
		public abstract bool TryAsNumber(object obj, out double value);

		public abstract bool IsStruct<T>(object obj) where T : struct;
		public abstract T AsStruct<T>(object obj) where T : struct;
		public abstract bool TryAsStruct<T>(object obj, out T value) where T : struct;

		public abstract bool IsClass<T>(object obj) where T : class;
		public abstract T AsClass<T>(object obj) where T : class;
		public abstract bool TryAsClass<T>(object obj, out T? value) where T : class;

		public abstract void Update(int elapsedMs);
	}
}
