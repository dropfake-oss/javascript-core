using System;

namespace Javascript.Core
{
	public interface IJavascriptLogger
	{
		void Debug(string format, params object?[] args);
		void Error(string format, params object?[] args);
	}

	public class DefaultLogger : IJavascriptLogger
	{
		public void Debug(string format, params object?[] args)
		{
			Console.Out.WriteLine(format, args);
		}

		public void Error(string format, params object?[] args)
		{
			Console.Error.WriteLine(format, args);
		}
	}

	// NUnit doesn't seem to want to allow Console.WriteLine to work on Tasks/Threads - even if we use TestContext.WriteLine(..)
	// so using this as a stopgap until someone figures out a better way
	public class StringLogger : IJavascriptLogger, IDisposable
	{
		System.Text.StringBuilder _buffer = new System.Text.StringBuilder();
		System.Text.StringBuilder _errorBuffer = new System.Text.StringBuilder();

		public void Debug(string format, params object?[] args)
		{
			//Console.Out.WriteLine(format, args);
			_buffer.AppendFormat(format, args);
			_buffer.AppendLine();
		}

		public void Flush()
		{
			Console.WriteLine(_buffer.ToString());
			Console.Error.WriteLine(_errorBuffer.ToString());
		}

		public void Dispose()
		{
			Flush();
		}

		public void Error(string format, params object?[] args)
		{
			//Console.Error.WriteLine(format, args);
			_errorBuffer.AppendFormat(format, args);
			_errorBuffer.AppendLine();
		}
	}
}
